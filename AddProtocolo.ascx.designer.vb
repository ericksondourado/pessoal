editar
editar gitlab - 06/01
'------------------------------------------------------------------------------
' <gerado automaticamente>
'     Este código foi gerado por uma ferramenta.
'
'     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
'     o código for recriado
' </gerado automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AddProtocolo

    '''<summary>
    '''Controle ScriptManager1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''Controle protocoloPasso1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents protocoloPasso1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle divProtocolo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents divProtocolo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle TableTipoProtocolo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TableTipoProtocolo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle ListTipoProtocolo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents ListTipoProtocolo As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Controle lblProduto.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblProduto As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle codProtocolo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents codProtocolo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle DivComplemento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents DivComplemento As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle LinkCodigoProtocoloOrigem.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LinkCodigoProtocoloOrigem As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''Controle UniSolicitante.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents UniSolicitante As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle txtDataRealizacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents txtDataRealizacao As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle codUnimedPrestador.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents codUnimedPrestador As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle HospitalPrestador.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents HospitalPrestador As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle CRMMedico.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents CRMMedico As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle medSolicitante.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents medSolicitante As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle equipeMedica.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents equipeMedica As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle UpdatePanelBeneficiario.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelBeneficiario As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Controle cartaoPac.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents cartaoPac As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle LabelUnimedSolicitante.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LabelUnimedSolicitante As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle LabelCodigoBeneficiarioFederacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LabelCodigoBeneficiarioFederacao As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle NomePaciente.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents NomePaciente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle NomeTitular.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents NomeTitular As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle dnPaciente.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents dnPaciente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle LabelIdade.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LabelIdade As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle txtValidadeCarteira.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents txtValidadeCarteira As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle txtPlano.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents txtPlano As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle tipoPlano.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents tipoPlano As Global.KAOS.WebControls.EditAutoComplete

    '''<summary>
    '''Controle UpdatePanelTISS.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelTISS As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Controle TableTISS.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TableTISS As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle RadioTipoAtendimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents RadioTipoAtendimento As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Controle TableInternacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TableInternacao As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle DataAdmissaoHospitalar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents DataAdmissaoHospitalar As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle txtDiariasSolicitadas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents txtDiariasSolicitadas As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle TableTipoAtendimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TableTipoAtendimento As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle ListTiposAtendimentoTISS.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents ListTiposAtendimentoTISS As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Controle RadioCaraterSolicitacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents RadioCaraterSolicitacao As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Controle ListTipoDoenca.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents ListTipoDoenca As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Controle txtTempoDoenca.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents txtTempoDoenca As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle ListTempo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents ListTempo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Controle txtIndicacaoClinica.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents txtIndicacaoClinica As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle divBotaoPasso1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents divBotaoPasso1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle btnContinuarPasso1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnContinuarPasso1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle labelErro.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents labelErro As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle divMensagens.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents divMensagens As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle TabMensagens1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TabMensagens1 As Global.IEPC.tabMensagens

    '''<summary>
    '''Controle divAnexos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents divAnexos As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle TabAnexos1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents TabAnexos1 As Global.IEPC.tabAnexos

    '''<summary>
    '''Controle dadosProtocoloResumidos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents dadosProtocoloResumidos As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle ascResumoDadosProtocolo1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents ascResumoDadosProtocolo1 As Global.IEPC.ascResumoDadosProtocolo

    '''<summary>
    '''Controle tabelaPasso2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents tabelaPasso2 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle UpdatePanelProcedimentos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelProcedimentos As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Controle Fieldset_Proc.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Fieldset_Proc As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle codProcedimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents codProcedimento As Global.KAOS.WebControls.EditAutoComplete

    '''<summary>
    '''Controle qtdeProcedimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents qtdeProcedimento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle valorProcedimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents valorProcedimento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle btnAddProcedimento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAddProcedimento As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle gridProcedimentos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents gridProcedimentos As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''Controle LabelErroProcedimentos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LabelErroProcedimentos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle UpdatePanelInsumos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelInsumos As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Controle Fieldset_Insumo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Fieldset_Insumo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle lblTituloMateriaisInsumos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblTituloMateriaisInsumos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle codInsumo2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents codInsumo2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle codInsumo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents codInsumo As Global.KAOS.WebControls.EditAutoComplete

    '''<summary>
    '''Controle qtdeInsumo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents qtdeInsumo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle btnAddInsumo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAddInsumo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle gridInsumos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents gridInsumos As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''Controle LabelErroInsumos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LabelErroInsumos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle UpdatePanelOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelOPME As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Controle FieldSet_OPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents FieldSet_OPME As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle tituloOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents tituloOPME As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle HiddenDadosOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents HiddenDadosOPME As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Controle codOPME2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents codOPME2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle qtdeOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents qtdeOPME As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle lblFabricanteOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblFabricanteOPME As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle txtFabricanteOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents txtFabricanteOPME As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle txtValorOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents txtValorOPME As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle btnAddOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAddOPME As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle gridOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents gridOPME As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''Controle LabelErroOPME.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LabelErroOPME As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle UpdatePanelCID.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelCID As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Controle FieldSet_CID.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents FieldSet_CID As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle ImagePesquisaCid.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents ImagePesquisaCid As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Controle CodigoCid.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents CodigoCid As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle NomeCid.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents NomeCid As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle codCID.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents codCID As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Controle btnAddCID.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAddCID As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle gridCID.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents gridCID As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''Controle divBotaoContinuar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents divBotaoContinuar As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle btnContinuar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnContinuar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle labelErroInclusaoProtocolo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents labelErroInclusaoProtocolo As Global.System.Web.UI.HtmlControls.HtmlGenericControl
End Class
